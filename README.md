# Mitt program

Dette er min semester oppgave 2, jeg har laget sjakk.

Sjakkspillet mitt er inspirert av en video serie på youtube laget av Software Architecture and Design.
Link til serien finner du her:
https://www.youtube.com/watch?v=h8fSdSUKttk&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&pp=iAQB

Jeg har sett til videoen for inspirasjon for oppsett av brikke, spiller, trekk og brett klassen, meg jeg har satt meg dypt inn i koden, og jobbet den inn i min egen. Ettersom at koden min bruker cell position, var det mye av koden fra videoene som kunne forenkles betraktelig, og andre deler som krevde litt ekstra arbeid. Jeg har testet koden godt gjennom arbeidet.

Jeg har markert hvilke metoder jeg har tatt, og hvilke som er inspirert med TODO's noen steder, get metodene er ikke markert ettersom at det er standard å legge inn. Det som ikke er markert har jeg lagd selv. Det er også kildeført øverst i filene der koden er inspirert eller tatt, hvor jeg også kort forklarer hva som er tatt og hva som er inspirert.

En viktig liten ting, størrelsen på spillvinduet må ikke endres, da slutter kontrolleren å fungere. Jeg prøvde å løse problemet som forårsaker det, men siden kontrolleren bare initialiseres en gang når man starter spillet, fungerte det ikke. Problemet ligger i at PixelToCellPositionConverter trenger dimensjonene til spillet for å funke, og får ikke til å oppdatere disse når størrelse på vinduet endres.

Når det kommer til å bruke spillet så har jeg lagd en liten video, den finner du her:
https://youtu.be/vDVqBOBr9Js
husk å like og kommentere videoen, og ikke vær redd for å se på noe av det andre jeg har laget

Om du ikke liker videoer har jeg også lagd en skriftlig forklaring.

Steg 1: Start spillet
Start med å åpne programmet i din kode editor, og gå inn på main filen, den heter ChessMain.java. Trykk så på Run, som da er den pilen øverst til høyre på skjermen, eller dra musen så langt opp du kan, og trykk på run der. Når du har fått kjørt spillet er det veldig enkelt å spille. 

Steg 2: Spill
Du velger brikken du ønsker å flytte ved å trykke på den. For å kunne flytte en brikke må du passe på at det er den brikken du prøver å flytte sin tur. Når du så trykke på brikken vil noen av plassene forran brikken lyse opp rødt, det betyr at brikken kan flytte seg dit. For å så få flyttet brikken dit er det bare å trykke på firkant du ønsker å flytte deg til som lyser rødt, så vil brikken flytte seg dit. Hvis brikken ikke flytter seg, kan det komme av at det trekket etterlater kongen din i sjakk, eller at kongen din allerede er i sjakk.

Steg Ekstra: noen ekstra ting
Det er to "special moves" som jeg har lagt inn fra vanlig sjakk, og dette er en passant, og castling. Jeg har ikke lagt inn pawn promotion dessverre, men dette kommer kanskje i en senere oppdatering. For å gjennomføre et en passant trekk derimot er dette veldig enkelt, alt som skal til er at din bonde er flyttet fram slik at om motstanderen din flytter en bonde to steg fram og ender opp på samme rad som deg, kan du flytte bonden din bak hans eller hennes, og ta den. Viktig å huske at dette bare kan gjøres runden etter at motstanderen flyttet sin bonde, hvis du venter en runde med å ta den, vil det ikke være mulig lenger. For å gjennomføre et castle trekk, må du passe på at kongen og tårnet ditt ikke har flyttet seg. Hvis de ikke har det, og plassene mellom tårnet og kongen er tomme, kongen ikke er i sjakk, ikke føres gjennom en sjakk eller ender i sjakk etter trekket, så kan du flytte kongen slik at den hopper over tårnet, i String vil det se slik ut: "K - - R" -> "- R K -".

Dette er da hvordan du spiller spillet mitt, har du noen spørsmål er det bare å gå i kommentar feltet på youtube og stille dem der. 