package no.uib.inf101.sem2.grid;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.pieces.Pawn;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Test is taken from GridCellTest in semester assignment 1 Tetris

public class TestGridCell {

    @Test
    void sanityTest() {
        Piece item = null;
        CellPosition pos = new CellPosition(4, 2);
        GridCell<String> gridCell = new GridCell<>(pos, item);

        assertEquals(pos, gridCell.pos());
        assertEquals(item, gridCell.piece());
    }

    @Test
    void gridCellEqualityAndHashCodeTest() {
        Piece item = null;
        CellPosition pos = new CellPosition(4, 2);
        GridCell<String> gridCell = new GridCell<>(pos, item);

        Piece item2 = null;
        CellPosition pos2 = new CellPosition(4, 2);
        GridCell<String> gridCell2 = new GridCell<>(pos2, item2);

        assertTrue(gridCell2.equals(gridCell));
        assertTrue(gridCell.equals(gridCell2));
        assertTrue(Objects.equals(gridCell, gridCell2));
        assertTrue(gridCell.hashCode() == gridCell2.hashCode());
    }

    @Test
    void gridCellInequalityTest() {
        Piece item = null;
        CellPosition pos = new CellPosition(4, 2);
        GridCell<String> gridCell = new GridCell<>(pos, item);

        Piece item2 = new Pawn(pos, null);
        CellPosition pos2 = new CellPosition(2, 4);

        GridCell<String> gridCell2 = new GridCell<>(pos2, item);
        GridCell<String> gridCell3 = new GridCell<>(pos, item2);

        assertFalse(gridCell2.equals(gridCell));
        assertFalse(gridCell.equals(gridCell2));
        assertFalse(gridCell.equals(gridCell3));
        assertFalse(gridCell2.equals(gridCell3));
        assertFalse(Objects.equals(gridCell, gridCell2));
        assertFalse(Objects.equals(gridCell, gridCell3));
        assertFalse(Objects.equals(gridCell2, gridCell3));
    }
}
