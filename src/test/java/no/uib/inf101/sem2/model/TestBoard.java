package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.chess.model.pieces.*;

public class TestBoard {

    @Test
    public void testChangedPrettyBoard(){
        Queen queen = new Queen(new CellPosition(1, 4), Alliance.BLACK);
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setPiece(queen);
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        String expected = String.join("\n", new String[] {
            "- - - - k - - -",
            "- - - - q - - -",
            "- - - - - - - -",
            "- - - - - - - -",
            "- - - - - - - -",
            "- - - - - - - -",
            "- - - - - - - -",
            "- - - - K - - -"
        });
        assertEquals(expected, board.prettyString());
    }

    @Test
    public void testPrettyBoard(){
        ChessBoard board = ChessBoard.createStandardBoard();
        String expected = String.join("\n", new String[] {
            "r n b q k b n r",
            "p p p p p p p p",
            "- - - - - - - -",
            "- - - - - - - -",
            "- - - - - - - -",
            "- - - - - - - -",
            "P P P P P P P P",
            "R N B Q K B N R"
        });
        assertEquals(expected, board.prettyString());
    }
}
