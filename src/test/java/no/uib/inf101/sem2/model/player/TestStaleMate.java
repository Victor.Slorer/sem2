package no.uib.inf101.sem2.model.player;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Queen;
import no.uib.inf101.grid.CellPosition;

public class TestStaleMate {
    
    @Test
    public void testStaleMate() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(6, 6), Alliance.WHITE));
        builder.setPiece(new Queen(new CellPosition(6, 2), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.BLACK));
        builder.setMoveMaker(Alliance.BLACK);
        ChessBoard board = builder.build();

        assertTrue(board.getCurrentPlayer().isInStaleMate());
    }
}
