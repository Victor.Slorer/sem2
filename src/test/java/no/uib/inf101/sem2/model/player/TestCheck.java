package no.uib.inf101.sem2.model.player;

import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.pieces.*;
import no.uib.inf101.grid.CellPosition;

public class TestCheck {
    
    @Test
    public void testPawnCheck(){

        final ChessBoard.Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Pawn(new CellPosition(1, 3), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertTrue(board.getBlackPlayer().isInCheck());
    }

    @Test
    public void testKnightCheck(){

        final ChessBoard.Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Knight(new CellPosition(1, 2), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertTrue(board.getBlackPlayer().isInCheck());
    }

    @Test
    public void testBishopCheck(){

        final ChessBoard.Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Bishop(new CellPosition(3, 1), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertTrue(board.getBlackPlayer().isInCheck());
    }

    @Test
    public void testRookCheck(){

        final ChessBoard.Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Rook(new CellPosition(6, 4), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertTrue(board.getBlackPlayer().isInCheck());
    }

    @Test
    public void testDiagonalQueenCheck(){

        final ChessBoard.Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Queen(new CellPosition(4, 0), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertTrue(board.getBlackPlayer().isInCheck());
    }

    @Test
    public void testStraightQueenCheck(){

        final ChessBoard.Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Queen(new CellPosition(1, 4), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.BLACK);
        ChessBoard board = builder.build();

        assertTrue(board.getBlackPlayer().isInCheck());
    }
}
