package no.uib.inf101.sem2.model.player;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.grid.CellPosition;

public class TestPlayerMakeMove {

    @Test
    public void testMovePawn() {
        ChessBoard board = ChessBoard.createStandardBoard();
        MoveTransition transition = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(6, 4), new CellPosition(4, 4)));

        assertTrue(transition.getMoveStatus().isLegal());
    }

    @Test
    public void testIllegalPawnMove() {
        ChessBoard board = ChessBoard.createStandardBoard();
        MoveTransition transition = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(6, 4), new CellPosition(4, 3)));

        assertTrue(!transition.getMoveStatus().isLegal());
    }
}
