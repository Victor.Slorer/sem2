package no.uib.inf101.sem2.model.player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.chess.model.pieces.*;
import no.uib.inf101.grid.CellPosition;

public class TestCheckMate {
    
    @Test
    public void testLadderCheckMate(){
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 7), Alliance.BLACK));
        builder.setPiece(new Rook(new CellPosition(2, 0), Alliance.WHITE));
        builder.setPiece(new Rook(new CellPosition(1, 1), Alliance.WHITE));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition transition = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(2, 0), new CellPosition(0, 0)));

        assertTrue(transition.getMoveStatus().isLegal());
        assertTrue(transition.getBoard().getCurrentPlayer().isInCheckMate());
    }

    @Test 
    public void testFastestPossibleMate() {
        ChessBoard board = ChessBoard.createStandardBoard();

        MoveTransition t1 = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(6, 5), new CellPosition(4, 5)));

        assertTrue(t1.getMoveStatus().isLegal());

        MoveTransition t2 = t1.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t1.getBoard(), new CellPosition(1, 4), new CellPosition(3, 4)));
        
        assertTrue(t2.getMoveStatus().isLegal());

        MoveTransition t3 = t2.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t2.getBoard(), new CellPosition(6, 6), new CellPosition(4, 6)));
        
        assertTrue(t3.getMoveStatus().isLegal());

        MoveTransition t4 = t3.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t3.getBoard(), new CellPosition(0, 3), new CellPosition(4, 7)));
        
        assertTrue(t4.getMoveStatus().isLegal());
        
        String expected = String.join("\n", new String[] {
            "r n b - k b n r",
            "p p p p - p p p",
            "- - - - - - - -",
            "- - - - p - - -",
            "- - - - - P P q",
            "- - - - - - - -",
            "P P P P P - - P",
            "R N B Q K B N R"
        });

        assertEquals(expected, t4.getBoard().prettyString());
        assertTrue(t4.getBoard().getCurrentPlayer().isInCheckMate());
    }

    @Test
    public void testScholarsMate() {
        ChessBoard board = ChessBoard.createStandardBoard();

        MoveTransition t1 = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(6, 4), new CellPosition(4, 4)));

        assertTrue(t1.getMoveStatus().isLegal());

        MoveTransition t2 = t1.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t1.getBoard(), new CellPosition(1, 4), new CellPosition(3, 4)));

        assertTrue(t2.getMoveStatus().isLegal());

        MoveTransition t3 = t2.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t2.getBoard(), new CellPosition(7, 5), new CellPosition(4, 2)));

        assertTrue(t3.getMoveStatus().isLegal());

        MoveTransition t4 = t3.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t3.getBoard(), new CellPosition(0, 1), new CellPosition(2, 2)));

        assertTrue(t4.getMoveStatus().isLegal());

        MoveTransition t5 = t4.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t4.getBoard(), new CellPosition(7, 3), new CellPosition(3, 7)));

        assertTrue(t5.getMoveStatus().isLegal());

        MoveTransition t6 = t5.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t5.getBoard(), new CellPosition(0, 6), new CellPosition(2, 5)));

        assertTrue(t6.getMoveStatus().isLegal());

        MoveTransition t7 = t6.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t6.getBoard(), new CellPosition(3, 7), new CellPosition(1, 5)));

        assertTrue(t7.getMoveStatus().isLegal());
        assertTrue(t7.getBoard().getCurrentPlayer().isInCheckMate());
    }

    protected List<String> escapeMovesPiece(ChessBoard board, List<Move> legalMoves){
        List<String> list =  new ArrayList<>();
        for (Move move : legalMoves) {
            MoveTransition transition = board.getCurrentPlayer().makeMove(move);
            if (transition.getMoveStatus().isLegal()) {
                list.add(move.getMoveType());
            }
        }
        return list;
    }

    protected List<CellPosition> escapeMovesPos(ChessBoard board, List<Move> legalMoves){
        List<CellPosition> list =  new ArrayList<>();
        for (Move move : legalMoves) {
            MoveTransition transition = board.getCurrentPlayer().makeMove(move);
            if (transition.getMoveStatus().isLegal()) {
                list.add(move.getCurrentPos());
                list.add(move.getDestination());
            }
        }
        return list;
    }
}
