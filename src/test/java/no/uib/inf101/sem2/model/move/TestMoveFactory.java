package no.uib.inf101.sem2.model.move;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Pawn;
import no.uib.inf101.chess.model.pieces.Queen;
import no.uib.inf101.chess.model.pieces.Rook;
import no.uib.inf101.grid.CellPosition;

public class TestMoveFactory {

    @Test
    public void testGetQueenMove() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(0, 2), Alliance.WHITE));
        builder.setPiece(new Queen(new CellPosition(4, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        Move move = MoveFactory.getMove(board, new CellPosition(4, 4), new CellPosition(0, 0));

        assertTrue(move != null);
    }

    @Test
    public void testGetIllegalQueenMove() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(0, 2), Alliance.WHITE));
        builder.setPiece(new Queen(new CellPosition(4, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        Move move = MoveFactory.getMove(board, new CellPosition(4, 4), new CellPosition(2, 0));

        assertTrue(move == null);
    }

    @Test
    public void testGetAttackingQueenMove() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(0, 2), Alliance.WHITE));
        builder.setPiece(new Queen(new CellPosition(4, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        Move move = MoveFactory.getMove(board, new CellPosition(4, 4), new CellPosition(0, 4));

        assertEquals("AttackMove", move.getMoveType());
    }

    @Test
    public void testGetCastleMove() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setPiece(new Rook(new CellPosition(7, 0), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        Move move = MoveFactory.getMove(board, new CellPosition(7, 4), new CellPosition(7, 2));

        assertEquals("CastleMove", move.getMoveType());
    }

    @Test
    public void testGetPawnDoubleStep() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setPiece(new Pawn(new CellPosition(6, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        Move move = MoveFactory.getMove(board, new CellPosition(6, 4), new CellPosition(4, 4));

        assertEquals("PawnDoubleStep", move.getMoveType());
    }
}
