package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.controller.ControllableChessModel;
import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessModel;

public class TestModel {
    
    @Test
    public void testCCM(){
        ChessBoard board = ChessBoard.createStandardBoard();
        ControllableChessModel model = new ChessModel(board);

        assertEquals(8, model.getDimensions().cols());
    }
}
