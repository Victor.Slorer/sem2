package no.uib.inf101.sem2.model.player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Rook;
import no.uib.inf101.grid.CellPosition;

public class TestCastle {

    @Test
    public void whiteKingSideCastle() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setPiece(new Rook(new CellPosition(7, 7), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer()
                .makeMove(MoveFactory.getMove(board, new CellPosition(7, 4), new CellPosition(7, 6)));
        
        assertTrue(t1.getMoveStatus().isLegal());
        assertEquals("CastleMove", t1.getMove().getMoveType());
    }

    @Test
    public void whiteQueenSideCastle() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setPiece(new Rook(new CellPosition(7, 0), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer()
                .makeMove(MoveFactory.getMove(board, new CellPosition(7, 4), new CellPosition(7, 2)));
        
        assertTrue(t1.getMoveStatus().isLegal());
        assertEquals("CastleMove", t1.getMove().getMoveType());
    }

    @Test
    public void blackKingSideCastle() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Rook(new CellPosition(0, 7), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.BLACK);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer()
                .makeMove(MoveFactory.getMove(board, new CellPosition(0, 4), new CellPosition(0, 6)));
        
        assertTrue(t1.getMoveStatus().isLegal());
        assertEquals("CastleMove", t1.getMove().getMoveType());
    }

    @Test
    public void blackQueenSideCastle() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new Rook(new CellPosition(0, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.setMoveMaker(Alliance.BLACK);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer()
                .makeMove(MoveFactory.getMove(board, new CellPosition(0, 4), new CellPosition(0, 2)));
        
        assertTrue(t1.getMoveStatus().isLegal());
        assertEquals("CastleMove", t1.getMove().getMoveType());
    }
}
