package no.uib.inf101.sem2.model.pieces;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Pawn;
import no.uib.inf101.grid.CellPosition;

public class TestPawn {

    @Test 
    public void testDoubleStep() {
        Builder builder =  new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 0), Alliance.WHITE));
        builder.setPiece(new Pawn(new CellPosition(6, 1), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(6, 1), new CellPosition(4, 1)));

        assertTrue(t1.getMoveStatus().isLegal());
    }
    
    @Test
    public void testEnPassant() {
        Pawn pawn = new Pawn(new CellPosition(3, 3), Alliance.WHITE);
        Builder builder =  new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(7, 0), Alliance.WHITE));
        builder.setPiece(new Pawn(new CellPosition(1, 4), Alliance.BLACK));
        builder.setPiece(pawn);
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(7, 0), new CellPosition(7, 1)));
        
        assertTrue(t1.getMoveStatus().isLegal());
        assertTrue(t1.getBoard().getEnPassantPawn() == null);

        MoveTransition t2 = t1.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t1.getBoard(), new CellPosition(1, 4), new CellPosition(3, 4)));
        
        assertTrue(t2.getMoveStatus().isLegal());
        assertEquals("PawnDoubleStep", t2.getMove().getMoveType());
        assertTrue(t2.getBoard().getEnPassantPawn() != null);
        assertTrue(t2.getBoard().getEnPassantPawn().getPiecePosition().row() == 3);

        MoveTransition t3 = t2.getBoard().getCurrentPlayer().makeMove(
            MoveFactory.getMove(t2.getBoard(), new CellPosition(3, 3), new CellPosition(2, 4)));
        
        assertTrue(t3.getMoveStatus().isLegal());
    }
}
