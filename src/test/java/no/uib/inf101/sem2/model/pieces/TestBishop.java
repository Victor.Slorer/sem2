package no.uib.inf101.sem2.model.pieces;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.Bishop;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

public class TestBishop {
    
    @Test
    public void TestBishopOnBoard(){
        Piece bishop = new Bishop(new CellPosition(0, 0), Alliance.WHITE);
        Builder builder =  new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(1, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(2, 0), Alliance.WHITE));
        builder.setPiece(bishop);
        builder.setMoveMaker(Alliance.WHITE);

        ChessBoard board = builder.build();

        assertTrue(board.getPiece(new CellPosition(0, 0)) == bishop);
    }

    @Test
    public void testAttackingMove(){
        
        Piece bishop = new Bishop(new CellPosition(4, 4), Alliance.WHITE);
        Piece bishop2 = new Bishop(new CellPosition(1, 1), Alliance.BLACK);
        Piece bishop3 = new Bishop(new CellPosition(1, 7), Alliance.BLACK);
        Piece bishop4 = new Bishop(new CellPosition(7, 7), Alliance.BLACK);
        Piece bishop5 = new Bishop(new CellPosition(7, 1), Alliance.BLACK);

        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(1, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(2, 0), Alliance.WHITE));
        builder.setPiece(bishop);
        builder.setPiece(bishop2);
        builder.setPiece(bishop3);
        builder.setPiece(bishop4);
        builder.setPiece(bishop5);
        builder.setMoveMaker(Alliance.WHITE);

        ChessBoard board = builder.build();

        List<Move> legalMoves = bishop.calculateLegalMoves(board);
        CellPosition pos1 = new CellPosition(1, 1);
        CellPosition pos2 = new CellPosition(1, 7);
        CellPosition pos3 = new CellPosition(7, 7);
        CellPosition pos4 = new CellPosition(7, 1);

        assertTrue(pos1.equals(legalMoves.get(11).getDestination()));
        assertTrue(pos2.equals(legalMoves.get(5).getDestination()));
        assertTrue(pos3.equals(legalMoves.get(2).getDestination()));
        assertTrue(pos4.equals(legalMoves.get(8).getDestination()));

        assertTrue("AttackMove" == legalMoves.get(11).getMoveType());
        assertTrue("AttackMove" == legalMoves.get(5).getMoveType());
        assertTrue("AttackMove" == legalMoves.get(2).getMoveType());
        assertTrue("AttackMove" == legalMoves.get(8).getMoveType());

    }
}
