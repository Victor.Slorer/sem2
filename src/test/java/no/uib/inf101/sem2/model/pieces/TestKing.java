package no.uib.inf101.sem2.model.pieces;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.grid.CellPosition;

public class TestKing {
    
    @Test
    public void instantiateKing() {
        King king = new King(new CellPosition(0, 2), Alliance.WHITE);
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(king);
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertEquals(king, board.getPiece(new CellPosition(0, 2)));
    }

    @Test
    public void moveKing() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(0, 2), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(0, 2), new CellPosition(0, 1)));

        assertTrue(t1.getMoveStatus().isLegal());
    }

    @Test
    public void testIllegalMove() {
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(0, 2), Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        MoveTransition t1 = board.getCurrentPlayer().makeMove(
            MoveFactory.getMove(board, new CellPosition(0, 2), new CellPosition(0, 3)));

        assertFalse(t1.getMoveStatus().isLegal());
    }
}
