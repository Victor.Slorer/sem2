package no.uib.inf101.sem2.model.pieces;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.move.AttackMove;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Knight;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

public class TestKnight {

    @Test
    public void testKnightOnBoard(){
        Piece knight = new Knight(new CellPosition(3, 4), Alliance.WHITE);
        Builder builder =  new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(1, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(2, 0), Alliance.WHITE));
        builder.setPiece(knight);
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        assertTrue(board.getPiece(new CellPosition(3, 4)) == knight);
    }

    @Test
    public void testLegalMoves(){
        Piece knight = new Knight(new CellPosition(3, 4), Alliance.WHITE);
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(1, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(2, 0), Alliance.WHITE));
        builder.setPiece(knight);
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        System.out.println(knight.calculateLegalMoves(board));
    }

    @Test
    public void testAttackingMove(){
        Piece knight = new Knight(new CellPosition(3, 4), Alliance.WHITE);
        Piece knight2 = new Knight(new CellPosition(1, 3), Alliance.BLACK);
        Builder builder = new Builder(8, 8);
        builder.setPiece(new King(new CellPosition(1, 0), Alliance.BLACK));
        builder.setPiece(new King(new CellPosition(2, 0), Alliance.WHITE));
        builder.setPiece(knight);
        builder.setPiece(knight2);
        builder.setMoveMaker(Alliance.WHITE);
        ChessBoard board = builder.build();

        List<Move> legalMoves = knight.calculateLegalMoves(board);
        Move move = new AttackMove(board, knight, new CellPosition(1, 3), knight2);

        assertTrue(move.getDestination().equals(legalMoves.get(1).getDestination()));
    }
}
