package no.uib.inf101.chess.controller;

import no.uib.inf101.chess.model.GameState;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

public interface ControllableChessModel {

    /**
     * Gets the dimensions of the tetris board
     * 
     * @return the dimensions of the board
     */
    GridDimension getDimensions();

    /**
     * Makes a move to the destination
     * 
     * @param destination destination of move
     */
    void playerMakeMove(CellPosition destination);

    /**
     * Selects the piece at the pressed position
     * on the board
     * 
     * @param pos pressed position on the board
     */
    void selectPiece(CellPosition pos);

    /**
     * Checks if the pressed position
     * on the board holds one of the
     * current players pieces
     * 
     * @param pos position pressed on board
     * @return true if there is a piece at
     *         the position, false otherwise
     */
    boolean pieceAtPosition(CellPosition pos);

    /**
     * Checks if the pressed tile is one
     * of the selected pieces legal moves
     * 
     * @param pos possition pressed on board
     * @return true if the position is a legal move,
     *         false otherwise
     */
    boolean pressedTileIsMove(CellPosition pos);

    /**
     * Gets the state of the game 
     * represented by the enum GameState
     * 
     * @return GameState
     */
    GameState getGameState();

    void resetBoard();
}
