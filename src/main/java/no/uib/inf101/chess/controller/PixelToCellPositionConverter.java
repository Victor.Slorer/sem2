package no.uib.inf101.chess.controller;

import java.awt.Point;

import no.uib.inf101.chess.view.ChessView;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

/**
 * Tok koden fra wack-a-mole fra øve eksamen i inf100
 * Endret litt der det trengtes
 */

public class PixelToCellPositionConverter {
    private double gameWidth;
    private double gameHeight;
    private GridDimension gd;
    private double appWidth;
    private double appHeight;
    private double appMargin;

    public PixelToCellPositionConverter(ChessView view, GridDimension gd) {
        this.appMargin = view.getAppMargin();
        this.appWidth = view.getAppWidth();
        this.appHeight = view.getAppHeight();
        this.gameHeight = this.appHeight - (2 * this.appMargin);
        this.gameWidth = this.appWidth - (2 * this.appMargin);
        this.gd = gd;
    }

    /**
     * Calculates the cell position of the point pressed on the screen
     * will return (-1, -1) if the point is not on the board
     * 
     * @param point pressed on the screen
     * 
     * @return the cell position pressed
     */
    public CellPosition getCellPosition(Point point) {
        double x = point.getX();
        double y = point.getY();

        if (!pointOnBoard(x, y)) {
            return new CellPosition(-1, -1);
        }

        double rows = this.gd.rows();
        double cols = this.gd.cols();
        double cellHeight = this.gameHeight / rows;
        double cellWidth = this.gameWidth / cols;

        int row = (int) ((y - this.appMargin) / cellHeight);
        int col = (int) ((x - this.appMargin) / cellWidth);

        return new CellPosition(row, col);
    }

    private boolean pointOnBoard(double x, double y) {
        return (((this.appMargin <= x && x <= this.appWidth - this.appMargin) &&
                (this.appMargin <= y && y <= this.appHeight - this.appMargin)));
    }
}
