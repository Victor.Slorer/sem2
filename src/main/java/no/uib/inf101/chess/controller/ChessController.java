package no.uib.inf101.chess.controller;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import no.uib.inf101.chess.view.ChessView;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.chess.model.GameState;

public class ChessController implements MouseListener {
  private ChessView view;
  private ControllableChessModel ccm;
  private PixelToCellPositionConverter converter;

  private Point mousePressedPoint;
  private int mousePressedCounter;

  public ChessController(ControllableChessModel ccm, ChessView view) {
    this.ccm = ccm;
    this.view = view;
    this.converter = new PixelToCellPositionConverter(view, ccm.getDimensions());

    view.addMouseListener(this);
    view.setFocusable(true);
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (ccm.getGameState() == GameState.ACTIVE_GAME) {
      mousePressedPoint = e.getPoint();
      mousePressedCounter++;
      playerMove();
    } else if (ccm.getGameState() != GameState.ACTIVE_GAME) {
      ccm.resetBoard();
    }
    view.repaint();
  }

  @Override
  public void mousePressed(MouseEvent e) {
  }

  @Override
  public void mouseReleased(MouseEvent e) {
  }

  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }

  private void playerMove() {
    if (mousePressedCounter == 1) {
      CellPosition pressedCell1 = converter.getCellPosition(mousePressedPoint);
      if (!pressedCell1.equals(new CellPosition(-1, -1))) {
        firstPress(pressedCell1);
      } else {
        mousePressedCounter = 0;
      }
    } else if (mousePressedCounter == 2) {
      CellPosition pressedCell2 = converter.getCellPosition(mousePressedPoint);
      if (ccm.pressedTileIsMove(pressedCell2)) {
        secondPress(pressedCell2);
      } else {
        mousePressedCounter = 1;
        firstPress(pressedCell2);
      }
    } else {
      mousePressedCounter = 0;
    }
  }

  private void firstPress(CellPosition pos) {
    System.out.println(pos);
    ccm.selectPiece(pos);
    if (!ccm.pieceAtPosition(pos)) {
      mousePressedCounter = 0;
    }
  }

  private void secondPress(CellPosition pos) {
    System.out.println(pos);
    ccm.playerMakeMove(pos);
    mousePressedCounter = 0;
  }
}
