package no.uib.inf101.chess.model;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.pieces.*;
import no.uib.inf101.chess.model.player.BlackPlayer;
import no.uib.inf101.chess.model.player.Player;
import no.uib.inf101.chess.model.player.WhitePlayer;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;
import no.uib.inf101.grid.GridCell;

/**
 * INSPIRED/TAKEN
 * The ChessBoard class was inspired by these four videos
 * had to change a couple of things to make it work with my code
 * the code for calculating the pieces and legal moves are also mine
 * and the code for the prettystring function
 * 
 * source1: https://www.youtube.com/watch?v=Cm70y54cDIo&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=14&ab_channel=SoftwareArchitecture%26Design
 * source2: https://www.youtube.com/watch?v=EVwrYFI74MA&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=15&ab_channel=SoftwareArchitecture%26Design
 * source3: https://www.youtube.com/watch?v=MM6hU9Wcmfg&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=16&ab_channel=SoftwareArchitecture%26Design
 * source4: https://www.youtube.com/watch?v=DFqiOA1l2sw&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=17&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public class ChessBoard {
    private final Builder builder;
    private final List<List<GridCell<Piece>>> board;
    private final List<Piece> whitePieces;
    private final List<Piece> blackPieces;
    private final Pawn enPassantPawn;
    private final WhitePlayer whitePlayer;
    private final BlackPlayer blackPlayer;
    private final Player currentPlayer;

    public static final ChessBoard STANDARD_BOARD = createStandardBoardImpl();

    public ChessBoard(Builder builder) {
        this.builder = builder;
        this.enPassantPawn = builder.enPassantPawn;
        this.board = builder.getGrid();
        this.whitePieces = getActivePieces(board, Alliance.WHITE);
        this.blackPieces = getActivePieces(board, Alliance.BLACK);
        final List<Move> whiteLegalMoves = calculateLegalMoves(this.whitePieces);
        final List<Move> blackLegalMoves = calculateLegalMoves(this.blackPieces);
        this.whitePlayer = new WhitePlayer(this, whiteLegalMoves, blackLegalMoves);
        this.blackPlayer = new BlackPlayer(this, blackLegalMoves, whiteLegalMoves);
        this.currentPlayer = builder.nextMoveMaker.choosePlayer(this.whitePlayer, this.blackPlayer);
    }

    /**
     * Creates a string representation of the board
     * 
     * @return the string representation of the board
     */
    public String prettyString() {
        String string = "";
        int num = 0;
        for (List<GridCell<Piece>> row : this.board) {
            for (GridCell<Piece> cell : row) {
                num += 1;
                if (cell.piece() == null) {
                    string = string + "-";
                } else {
                    if (cell.piece().getPieceAlliance() == Alliance.BLACK) {
                        if (cell.piece().getPieceType() == "Knight") {
                            string = string + cell.piece().getPieceType().toLowerCase().charAt(1);
                        } else {
                            string = string + cell.piece().getPieceType().toLowerCase().charAt(0);
                        }
                    } else {
                        if (cell.piece().getPieceType() == "Knight") {
                            string = string + cell.piece().getPieceType().toUpperCase().charAt(1);
                        } else {
                            string = string + cell.piece().getPieceType().charAt(0);
                        }
                    }
                }
                if (num < 8) {
                    string = string + " ";
                }
            }
            if (row != this.board.get(this.board.size() - 1)) {
                string = string + "\n";
            }
            num = 0;
        }
        return string;
    }

    /**
     * Gets the white pieces on the board
     * 
     * @return the white pieces on the board
     */
    public List<Piece> getWhitePieces() {
        return this.whitePieces;
    }

    /**
     * Gets the black pieces on the board
     * 
     * @return the black pieces on the board
     */
    public List<Piece> getBlackPieces() {
        return this.blackPieces;
    }

    /**
     * Gets all the pieces on the board
     * 
     * @return all the pieces on the board
     */
    public List<Piece> getAllPieces() {
        List<Piece> list = new ArrayList<>();
        list.addAll(this.whitePieces);
        list.addAll(this.blackPieces);
        return list;
    }

    /**
     * Gets all the legal moves of the pieces on the board
     * 
     * @return all the legal moves of the pieces on the board
     */
    public List<Move> getAllLegalMoves() {
        List<Move> list = new ArrayList<>();
        list.addAll(this.whitePlayer.getLegalMoves());
        list.addAll(this.blackPlayer.getLegalMoves());
        return list;
    }

    /**
     * Gets the boards builder
     * 
     * @return the builder
     */
    public Builder getBuilder() {
        return this.builder;
    }

    /**
     * Checks if position is occupied using the builder
     * 
     * @param pos to be checked
     * 
     * @return true if pos is occupied, false if not
     */
    public boolean isPositionOccupied(CellPosition pos) {
        return this.builder.isPositionOccupied(pos);
    }

    /**
     * Checks if position is on grid using the builder
     * 
     * @param pos to be checked
     * 
     * @return true if position is on grid, false if not
     */
    public boolean positionIsOnGrid(CellPosition pos) {
        return this.builder.positionIsOnGrid(pos);
    }

    /**
     * Gets the white player
     * 
     * @return the white player
     */
    public Player getWhitePlayer() {
        return this.whitePlayer;
    }

    /**
     * Gets the black player
     * 
     * @return the black player
     */
    public Player getBlackPlayer() {
        return this.blackPlayer;
    }

    /**
     * Gets the current player
     * 
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

    /**
     * Returns the piece on the board at the given position
     * 
     * @param pos to get the piece on the board
     * 
     * @return the piece at the position
     */
    public Piece getPiece(CellPosition pos) {
        return this.builder.getPiece(pos);
    }

    /**
     * Gets the en passant pawn on the board
     * returns null if there are no en passant pawn
     * 
     * @return the en passant pawn
     */
    public Pawn getEnPassantPawn() {
        return this.enPassantPawn;
    }

    /**
     * Returns a standard chess board
     * 
     * @return the standard board
     */
    public static ChessBoard createStandardBoard() {
        return STANDARD_BOARD;
    }

    // TODO Taken from source2
    // Creates a standard chess board
    public static ChessBoard createStandardBoardImpl() {
        Builder builder = new Builder(8, 8);

        builder.set(new CellPosition(0, 0), new Rook(new CellPosition(0, 0), Alliance.BLACK));
        builder.set(new CellPosition(0, 1), new Knight(new CellPosition(0, 1), Alliance.BLACK));
        builder.set(new CellPosition(0, 2), new Bishop(new CellPosition(0, 2), Alliance.BLACK));
        builder.set(new CellPosition(0, 3), new Queen(new CellPosition(0, 3), Alliance.BLACK));
        builder.set(new CellPosition(0, 4), new King(new CellPosition(0, 4), Alliance.BLACK));
        builder.set(new CellPosition(0, 5), new Bishop(new CellPosition(0, 5), Alliance.BLACK));
        builder.set(new CellPosition(0, 6), new Knight(new CellPosition(0, 6), Alliance.BLACK));
        builder.set(new CellPosition(0, 7), new Rook(new CellPosition(0, 7), Alliance.BLACK));
        builder.set(new CellPosition(1, 0), new Pawn(new CellPosition(1, 0), Alliance.BLACK));
        builder.set(new CellPosition(1, 1), new Pawn(new CellPosition(1, 1), Alliance.BLACK));
        builder.set(new CellPosition(1, 2), new Pawn(new CellPosition(1, 2), Alliance.BLACK));
        builder.set(new CellPosition(1, 3), new Pawn(new CellPosition(1, 3), Alliance.BLACK));
        builder.set(new CellPosition(1, 4), new Pawn(new CellPosition(1, 4), Alliance.BLACK));
        builder.set(new CellPosition(1, 5), new Pawn(new CellPosition(1, 5), Alliance.BLACK));
        builder.set(new CellPosition(1, 6), new Pawn(new CellPosition(1, 6), Alliance.BLACK));
        builder.set(new CellPosition(1, 7), new Pawn(new CellPosition(1, 7), Alliance.BLACK));

        builder.set(new CellPosition(7, 0), new Rook(new CellPosition(7, 0), Alliance.WHITE));
        builder.set(new CellPosition(7, 1), new Knight(new CellPosition(7, 1), Alliance.WHITE));
        builder.set(new CellPosition(7, 2), new Bishop(new CellPosition(7, 2), Alliance.WHITE));
        builder.set(new CellPosition(7, 3), new Queen(new CellPosition(7, 3), Alliance.WHITE));
        builder.set(new CellPosition(7, 4), new King(new CellPosition(7, 4), Alliance.WHITE));
        builder.set(new CellPosition(7, 5), new Bishop(new CellPosition(7, 5), Alliance.WHITE));
        builder.set(new CellPosition(7, 6), new Knight(new CellPosition(7, 6), Alliance.WHITE));
        builder.set(new CellPosition(7, 7), new Rook(new CellPosition(7, 7), Alliance.WHITE));
        builder.set(new CellPosition(6, 0), new Pawn(new CellPosition(6, 0), Alliance.WHITE));
        builder.set(new CellPosition(6, 1), new Pawn(new CellPosition(6, 1), Alliance.WHITE));
        builder.set(new CellPosition(6, 2), new Pawn(new CellPosition(6, 2), Alliance.WHITE));
        builder.set(new CellPosition(6, 3), new Pawn(new CellPosition(6, 3), Alliance.WHITE));
        builder.set(new CellPosition(6, 4), new Pawn(new CellPosition(6, 4), Alliance.WHITE));
        builder.set(new CellPosition(6, 5), new Pawn(new CellPosition(6, 5), Alliance.WHITE));
        builder.set(new CellPosition(6, 6), new Pawn(new CellPosition(6, 6), Alliance.WHITE));
        builder.set(new CellPosition(6, 7), new Pawn(new CellPosition(6, 7), Alliance.WHITE));

        builder.setMoveMaker(Alliance.WHITE);

        return builder.build();
    }

    // TODO somewhat inspired, but code in the method is mine
    // Calculates the legal moves of a list of pieces
    private List<Move> calculateLegalMoves(List<Piece> pieces) {
        List<Move> moves = new ArrayList<>();
        for (Piece piece : pieces) {
            for (Move move : piece.calculateLegalMoves(this)) {
                moves.add(move);
            }
        }
        return moves;
    }

    // TODO somewhat inspired, but code in the method is mine
    // Gets the active pieces of a player on the board
    private static List<Piece> getActivePieces(List<List<GridCell<Piece>>> grid, Alliance alliance) {
        List<Piece> pieces = new ArrayList<>();
        for (List<GridCell<Piece>> row : grid) {
            for (GridCell<Piece> cell : row) {
                if (cell.piece() != null) {
                    if (cell.piece().getPieceAlliance() == alliance) {
                        pieces.add(cell.piece());
                    }
                }
            }
        }
        return pieces;
    }

    /**
     * INSPIRED/TAKEN
     * The code for the builder class was taken from this video,
     * however i changed it, making it extend the Grid class
     * 
     * source: https://www.youtube.com/watch?v=Cm70y54cDIo&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=14&ab_channel=SoftwareArchitecture%26Design
     */

    // Static class extending the grid, which is used to build the board
    public static class Builder extends Grid<Piece> {
        Alliance nextMoveMaker;
        Pawn enPassantPawn = null;

        public Builder(int rows, int cols) {
            super(rows, cols, null);
        }

        /**
         * places a piece in the gridcell on the board
         * this uses the set function from the Grid class
         * 
         * @param piece to be placed on the board
         * 
         * @return the builder with the placed piece
         */
        public Builder setPiece(Piece piece) {
            this.set(piece.getPiecePosition(), piece);
            return this;
        }

        /**
         * Sets the next move maker
         * 
         * @param nextMoveMaker the alliance of the next move maker
         * 
         * @return the builder with the next move maker
         */
        public Builder setMoveMaker(Alliance nextMoveMaker) {
            this.nextMoveMaker = nextMoveMaker;
            return this;
        }

        /**
         * Sets the en passant pawn
         * 
         * @param pawn the en passant pawn
         * 
         * @return the builder with the en passant pawn
         */
        public Builder setEnPassantPawn(Pawn pawn) {
            this.enPassantPawn = pawn;
            return this;
        }

        /**
         * Builds a chess board
         * 
         * @return a new Chess Board
         */
        public ChessBoard build() {
            return new ChessBoard(this);
        }
    }
}
