package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.pieces.Pawn;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

/**
 * INPSIRED
 * inspired by this video to create a separate move for the Pawn double step, to
 * keep track of the en passant pawn
 * 
 * source: https://www.youtube.com/watch?v=01fpv16NSJY&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=24&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public class PawnDoubleStep extends Move {

    public PawnDoubleStep(ChessBoard board, Pawn movedPiece, CellPosition destination) {
        super(board, movedPiece, destination);
    }

    @Override
    public String getMoveType() {
        return "PawnDoubleStep";
    }

    // TODO inspired
    @Override
    public ChessBoard execute() {
        final Builder builder = new Builder(8, 8);
        // Gets and sets all the pieces that weren't moved
        for (Piece piece : this.board.getCurrentPlayer().getActivePieces()) {
            if (!this.movedPiece.equals(piece)) {
                builder.setPiece(piece);
            }
        }
        for (Piece piece : this.board.getCurrentPlayer().getOpponent().getActivePieces()) {
            builder.setPiece(piece);
        }
        // creates a new pawn at the new location
        Pawn movedPawn = (Pawn) this.movedPiece.movePiece(this);
        builder.setPiece(movedPawn);
        // Sets the moved pawn as an enPassantPawn
        builder.setEnPassantPawn(movedPawn);
        // Sets the next moveMaker
        builder.setMoveMaker(this.board.getCurrentPlayer().getOpponent().getPlayerAlliance());

        // Builds and returns a new board
        return builder.build();
    }
}
