package no.uib.inf101.chess.model.pieces;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.AttackMove;
import no.uib.inf101.chess.model.move.MajorMove;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.grid.CellPosition;

/**
 * ISNPIRED
 * The Code for calculating the legal moves of the knight was inspired by this video
 * 
 * Source: https://www.youtube.com/watch?v=5SKOOG3TwVU&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=5&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public class Knight extends Piece {
    private final static CellPosition[] CANDIDATE_POSITIONS = { new CellPosition(-1, -2), new CellPosition(-2, -1),
            new CellPosition(-2, 1), new CellPosition(-1, 2), new CellPosition(1, 2),
            new CellPosition(2, 1), new CellPosition(2, -1), new CellPosition(1, -2), };

    public Knight(CellPosition pos, Alliance pieceAlliance) {
        super(pos, pieceAlliance, true);
    }

    public Knight(CellPosition pos, Alliance pieceAlliance, boolean firstMove) {
        super(pos, pieceAlliance, firstMove);
    }

    // TODO inspired
    @Override
    public List<Move> calculateLegalMoves(ChessBoard board) {
        CellPosition candidateDestinationPos;
        final List<Move> legalMoves = new ArrayList<>();

        for (CellPosition currentCandidate : CANDIDATE_POSITIONS) {
            int row = this.pos.row() + currentCandidate.row();
            int col = this.pos.col() + currentCandidate.col();
            candidateDestinationPos = new CellPosition(row, col);

            if (board.positionIsOnGrid(candidateDestinationPos)) {
                if (!board.isPositionOccupied(candidateDestinationPos)) {
                    legalMoves.add(new MajorMove(board, this, candidateDestinationPos));
                } else {
                    Piece pieceAtDestination = board.getPiece(candidateDestinationPos);
                    Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

                    if (this.pieceAlliance != pieceAlliance) {
                        legalMoves.add(new AttackMove(board, this, candidateDestinationPos, pieceAtDestination));
                    }
                }
            }
        }
        return legalMoves;
    }

    @Override
    public String getPieceType() {
        return "Knight";
    }

    @Override
    public Piece movePiece(Move move) {
        return new Knight(move.getDestination(), move.getMovedPiece().getPieceAlliance(), false);
    }

}
