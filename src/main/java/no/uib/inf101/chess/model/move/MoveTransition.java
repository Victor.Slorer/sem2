package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;

/**
 * TAKEN
 * The MoveTransition class was taken from this video
 * 
 * source: https://www.youtube.com/watch?v=lIstsXq_EMs&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=18&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public class MoveTransition {
    private final ChessBoard transitionBoard;
    private final Move move;
    private final MoveStatus moveStatus;

    // Used to create a new board, to check whether the move made was legal or not
    public MoveTransition(ChessBoard transitionBoard, Move move, MoveStatus moveStatus){
        this.transitionBoard = transitionBoard;
        this.move = move;
        this.moveStatus = moveStatus;
    }

    /**
     * Gets the transition board
     * 
     * @return transition board
     */
    public ChessBoard getBoard() {
        return this.transitionBoard;
    }

    /**
     * Gets the move that was made
     * 
     * @return the move
     */
    public Move getMove(){
        return this.move;
    }

    /**
     * Gets the MoveStatus
     * 
     * @return moveStatus
     */
    public MoveStatus getMoveStatus(){
        return this.moveStatus;
    }
}
