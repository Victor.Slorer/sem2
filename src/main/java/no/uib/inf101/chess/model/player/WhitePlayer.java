package no.uib.inf101.chess.model.player;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.chess.model.pieces.Rook;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.chess.model.move.CastleMove.KingSideCastleMove;
import no.uib.inf101.chess.model.move.CastleMove.QueenSideCastleMove;

public class WhitePlayer extends Player {

    public WhitePlayer(ChessBoard board, List<Move> legaMoves, List<Move> opponentMoves) {
        super(board, legaMoves, opponentMoves);
    }

    @Override
    public List<Piece> getActivePieces() {
        return this.board.getWhitePieces();
    }

    @Override
    public Alliance getPlayerAlliance() {
        return Alliance.WHITE;
    }

    @Override
    public Player getOpponent() {
        return this.board.getBlackPlayer();
    }

    @Override
    public List<Move> getKingCastleMoves(List<Move> legalMoves, List<Move> opponetMoves) {
        List<Move> castleMoves = new ArrayList<>();

        // Checks if the king can Castle
        if (!kingCanCastle()) {
            // return empty list if king can't castle
            return castleMoves;
        }

        // Checks that the king has not moved, and is in its initial position
        if (this.playerKing.isFirstMove() && this.playerKing.getPiecePosition().equals(new CellPosition(7, 4))) {

            // Checks that the positions between the king and the king side rook are not
            // occupied
            if (!this.board.isPositionOccupied(new CellPosition(7, 5))
                    && !this.board.isPositionOccupied(new CellPosition(7, 6))) {
                // Gets the piece at the kings rook position
                Piece kingsRook = this.board.getPiece(new CellPosition(7, 7));
                // Checks if the kings rook is a piece, if it has moved, and if the piece is a
                // rook
                if (kingsRook != null && kingsRook.isFirstMove() && kingsRook.getPieceType() == "Rook") {
                    // Checks if the positions between the rook and the king are under attack
                    if (!Player.isPosUnderAttack(new CellPosition(7, 5), opponetMoves)
                            && !Player.isPosUnderAttack(new CellPosition(7, 6), opponetMoves)) {
                        // Creates a new KingSideCastleMove
                        castleMoves.add(new KingSideCastleMove(this.board, this.playerKing, new CellPosition(7, 6),
                                (Rook) kingsRook, kingsRook.getPiecePosition(), new CellPosition(7, 5)));
                    }
                }
            }

            // Checks if the positions between the king and the queen side rook are occupied
            if (!this.board.isPositionOccupied(new CellPosition(7, 1))
                    && !this.board.isPositionOccupied(new CellPosition(7, 2))
                    && !this.board.isPositionOccupied(new CellPosition(7, 3))) {
                // Gets the piece at the queens rook position
                Piece queensRook = this.board.getPiece(new CellPosition(7, 0));
                // Checks if the queens rook is a piece, that it hasn't moved, and that it's a
                // rook
                if (queensRook != null && queensRook.isFirstMove() && queensRook.getPieceType() == "Rook") {
                    // Checks that the positions between the king and queens rook are not under
                    // attack
                    if (!Player.isPosUnderAttack(new CellPosition(7, 2), opponetMoves)
                            && !Player.isPosUnderAttack(new CellPosition(7, 3), opponetMoves)) {
                        // Creates and adds a new QueenSideCastleMove
                        castleMoves.add(new QueenSideCastleMove(this.board, this.playerKing, new CellPosition(7, 2),
                                (Rook) queensRook, queensRook.getPiecePosition(), new CellPosition(7, 3)));
                    }
                }
            }
        }

        return castleMoves;
    }

}
