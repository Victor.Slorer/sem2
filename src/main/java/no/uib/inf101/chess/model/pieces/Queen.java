package no.uib.inf101.chess.model.pieces;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.AttackMove;
import no.uib.inf101.chess.model.move.MajorMove;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.grid.CellPosition;

/**
 * INSPIRED
 * Combined the code from the Bishop and Rook
 */

public class Queen extends Piece {
    private final static CellPosition[] CANDIDATE_POSITIONS = { new CellPosition(-1, 0),
            new CellPosition(0, 1), new CellPosition(1, 0), new CellPosition(0, -1),
            new CellPosition(1, 1), new CellPosition(-1, 1), new CellPosition(1, -1),
            new CellPosition(-1, -1) };

    public Queen(final CellPosition pos, final Alliance pieceAlliance) {
        super(pos, pieceAlliance, true);
    }

    public Queen(final CellPosition pos, final Alliance pieceAlliance, boolean firstMove) {
        super(pos, pieceAlliance, firstMove);
    }

    @Override
    public String getPieceType() {
        return "Queen";
    }

    @Override
    public List<Move> calculateLegalMoves(ChessBoard board) {
        CellPosition candidateDestinationPos = this.pos;
        final List<Move> legalMoves = new ArrayList<>();

        for (CellPosition currentCandidate : CANDIDATE_POSITIONS) {
            int row = this.pos.row();
            int col = this.pos.col();

            while (board.positionIsOnGrid(candidateDestinationPos)) {
                row += currentCandidate.row();
                col += currentCandidate.col();
                candidateDestinationPos = new CellPosition(row, col);

                if (board.positionIsOnGrid(candidateDestinationPos)) {
                    if (!board.isPositionOccupied(candidateDestinationPos)) {
                        legalMoves.add(new MajorMove(board, this, candidateDestinationPos));
                    } else {
                        Piece pieceAtDestination = board.getPiece(candidateDestinationPos);
                        Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

                        if (this.pieceAlliance != pieceAlliance) {
                            legalMoves.add(new AttackMove(board, this, candidateDestinationPos, pieceAtDestination));
                        }
                        candidateDestinationPos = this.pos;
                        break;
                    }
                }
            }
            candidateDestinationPos = this.pos;
        }
        return legalMoves;
    }

    @Override
    public Piece movePiece(Move move) {
        return new Queen(move.getDestination(), move.getMovedPiece().getPieceAlliance(), false);
    }

}
