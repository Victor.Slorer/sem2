package no.uib.inf101.chess.model.move;

/**
 * TAKEN
 * The MoveStatus enum was taken from this video
 * 
 * source: https://www.youtube.com/watch?v=iBhZtNpV6r8&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=20&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public enum MoveStatus {
    LEGAL {
        @Override
        public boolean isLegal() {
            return true;
        }

        @Override
        public String getStatusType() {
            return "Legal move";
        }
    },    
    ILLEGAL_MOVE {
        @Override
        public boolean isLegal() {
            return false;
        }

        @Override
        public String getStatusType() {
            return "Illegal Move";
        }
    },
    LEADS_TO_PLAYER_IN_CHECK {
        @Override
        public boolean isLegal() {
            return false;
        }

        @Override
        public String getStatusType() {
            return "Leads to check";
        }
    };

    /**
     * Checks whether the move was legal based on the
     * MoveStatus
     * 
     * @return true if the move is legal
     */
    public abstract boolean isLegal();

    /**
     * Gets the MoveStatus as a String
     * 
     * @return the MoveStatus as a string
     */
    public abstract String getStatusType();
}
