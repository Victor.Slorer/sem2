package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.grid.CellPosition;


/**
 * MoveFactory was inspired by this video, i got the idea of creating one myself
 * during testing, since i needed a way to get the move that was to be made. 
 * I looked to the video for how to make it though.
 * 
 * source: https://www.youtube.com/watch?v=01fpv16NSJY&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=24&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */
// Used to create new moves when playing and testing the game
public final class MoveFactory {

    private MoveFactory() {
        throw new RuntimeException("Not instantiatable!");
    }

    public static Move getMove(final ChessBoard board,
            final CellPosition currentPos,
            final CellPosition destinationPos) {
        for (final Move move : board.getAllLegalMoves()) {
            if (move.getCurrentPos().equals(currentPos) &&
                    move.getDestination().equals(destinationPos)) {
                return move;
            }
        }
        return null;
    }
}
