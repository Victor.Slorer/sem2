package no.uib.inf101.chess.model.pieces;

import no.uib.inf101.chess.model.player.BlackPlayer;
import no.uib.inf101.chess.model.player.Player;
import no.uib.inf101.chess.model.player.WhitePlayer;

/**
 * TAKEN
 * The Alliance enum was taken from these three videos
 * 
 * The last two sources are for the getDirection and choosePlayer methods
 * source1: https://www.youtube.com/watch?v=GKJ3yYFCJO4&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=3&ab_channel=SoftwareArchitecture%26Design
 * source2: https://www.youtube.com/watch?v=ItdijoP99pQ&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=10&ab_channel=SoftwareArchitecture%26Design
 * source3: https://www.youtube.com/watch?v=JsI8K1ONcPU&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=21&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public enum Alliance {
    BLACK {
        @Override
        public int getDirection() {
            return 1;
        }

        @Override
        public boolean isWhite() {
            return false;
        }

        @Override
        public boolean isBlack() {
            return true;
        }

        @Override
        public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
            return blackPlayer;
        }
    },
    WHITE {
        @Override
        public int getDirection() {
            return -1;
        }

        @Override
        public boolean isWhite() {
            return true;
        }

        @Override
        public boolean isBlack() {
            return false;
        }

        @Override
        public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
            return whitePlayer;
        }
    };

    /**
     * Gets the pawn direction based on alliance
     * 
     * @return the pawn direction
     */
    public abstract int getDirection();

    /**
     * Checks if alliance is White
     * 
     * @return true if alliance is white, false if black
     */
    public abstract boolean isWhite();

    /**
     * Checks if alliance is black
     * 
     * @return true is alliance is black, false if white
     */
    public abstract boolean isBlack();

    /**
     * Returns the player current player based on alliance
     * 
     * @param whitePlayer the white player
     * @param blackPlayer the black player
     * @return The white player if alliance is white, black player if alliance is
     *         black
     */
    public abstract Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer);
}
