package no.uib.inf101.chess.model.player;

import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.move.MoveStatus;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.chess.model.pieces.Alliance;
import no.uib.inf101.chess.model.pieces.King;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

/**
 * The player class and the white and black player classes is inspired by these videos
 * Mostly for the structure, the code for calculating things for the different methods 
 * is mine, apart from the makeMove method which is more inspired, and the establish King
 * method and escapeMoves methods which are taken.
 * 
 * Inspired by the last video to keep the castle move calculations in the Player
 * class. I wrote the if sentences myself.
 * 
 * source1: https://www.youtube.com/watch?v=DFqiOA1l2sw&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=17&ab_channel=SoftwareArchitecture%26Design
 * source2: https://www.youtube.com/watch?v=lIstsXq_EMs&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=18&ab_channel=SoftwareArchitecture%26Design
 * source3: https://www.youtube.com/watch?v=CI5X4627ET0&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=19&ab_channel=SoftwareArchitecture%26Design
 * source4: https://www.youtube.com/watch?v=iBhZtNpV6r8&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=20&ab_channel=SoftwareArchitecture%26Design
 * source5: https://www.youtube.com/watch?v=smpICj66zsg&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=28&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public abstract class Player {
    protected final ChessBoard board;
    protected final King playerKing;
    protected final List<Move> legalMoves;
    protected final boolean isInCheck;

    Player(ChessBoard board, List<Move> legalMoves, List<Move> opponentMoves) {
        this.board = board;
        this.playerKing = establishKing();
        legalMoves.addAll(getKingCastleMoves(legalMoves, opponentMoves));
        this.legalMoves = legalMoves;
        this.isInCheck = isPosUnderAttack(this.playerKing.getPiecePosition(), opponentMoves);
    }

    // TODO taken
    // Establishes the players king
    private King establishKing() {
        for (Piece piece : getActivePieces()) {
            if (piece.getPieceType() == "King") {
                return (King) piece;
            }
        }
        throw new RuntimeException("Not a valid board");
    }

    /**
     * gets the players king
     * 
     * @return the players king
     */
    public King getPlayerKing() {
        return this.playerKing;
    }

    /**
     * Gets the players legal moves
     * 
     * @return the players legal moves
     */
    public List<Move> getLegalMoves() {
        return this.legalMoves;
    }

    /**
     * Checks if the given move is legal
     * 
     * @param move to be checked
     * 
     * @return true if the move is legal, false otherwise
     */
    public boolean isLegalMove(Move move) {
        return this.legalMoves.contains(move);
    }

    /**
     * returns the isInCheck boolean, which is true if the player king is in check,
     * false otherwise
     * 
     * @return the isInCheck boolean
     */
    public boolean isInCheck() {
        return this.isInCheck;
    }

    /**
     * Checks if the player is in check mate, by checking if the player is in check
     * and has no escape moves
     * 
     * @return true if the player is in check mate, false otherwise
     */
    public boolean isInCheckMate() {
        return this.isInCheck && !escapeMoves();
    }

    /**
     * Checks if the player is in stale mate, by checking if the player is not in
     * check, and has no other legal moves
     * 
     * @return true if the player is in stale mate, false other wise
     */
    public boolean isInStaleMate() {
        return !this.isInCheck && !escapeMoves();
    }

    /**
     * Checks if the player king is castled
     * 
     * @return true if the player king is castled, false otherwise
     */
    public boolean isCastled() {
        return this.playerKing.isCastled();
    }

    /**
     * Checks if the position is under attack by the opponent
     * 
     * @param piecePos      position to be checked if under attack
     * @param opponentMoves the opponents moves
     * @return true if the position is under attack, false if not
     */
    static boolean isPosUnderAttack(CellPosition piecePos, List<Move> opponentMoves) {
        for (Move move : opponentMoves) {
            if (piecePos.equals(move.getDestination())) {
                return true;
            }
        }
        return false;
    }

    // Checks if the player has escape moves by creating a new MoveTransion with the
    // players possible legal moves
    // If the MoveStatus of the MoveTransition is legal, then the player has escape
    // moves, otherwise the player has no escape moves
    //TODO Taken
    private boolean escapeMoves() {
        for (Move move : this.legalMoves) {
            MoveTransition transition = this.board.getCurrentPlayer().makeMove(move);
            if (transition.getMoveStatus().isLegal()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the king can castle
     * @return
     */
    public boolean kingCanCastle() {
        if (!this.isInCheck && !this.playerKing.isCastled()) {
            return true;
        }
        return false;
    }

    /**
     * Creates a new MoveTransition with the given move
     * 
     * First checks if the move is legal, if not, then the MoveStatus is set to
     * Illegal Move and the MoveTransition is returned with the original board
     * 
     * Then executes the move and creates a transition board
     * 
     * If the move leads to the player being in check, then the MoveStatus is 
     * set to LEADS_TO_PLAYER_IN_CHECK and the MoveTransition is returned
     * with the original board
     * 
     * Otherwise the move is legal, and the MoveTransition is returned with the
     * new board, and the MoveStatus is set to LEGAL_MOVE
     * 
     * @param move the player made
     * 
     * @return MoveTransition with the move made
     */
    // TODO inspired
    public MoveTransition makeMove(Move move) {
        if (!isLegalMove(move)) {
            return new MoveTransition(this.board, move, MoveStatus.ILLEGAL_MOVE);
        }

        final ChessBoard transitionBoard = move.execute();
        if (transitionBoard.getCurrentPlayer().getOpponent().isInCheck()) {
            return new MoveTransition(this.board, move, MoveStatus.LEADS_TO_PLAYER_IN_CHECK);
        }

        return new MoveTransition(transitionBoard, move, MoveStatus.LEGAL);
    }

    /**
     * Gets the players active pieces
     *
     * @return the players active pieces
     */
    public abstract List<Piece> getActivePieces();

    /**
     * Gets the players alliance
     * 
     * @return the players alliance
     */
    public abstract Alliance getPlayerAlliance();

    /**
     * Gets the players opponent
     * 
     * @return gets the players opponent
     */
    public abstract Player getOpponent();

    /**
     * Calculates, then gets the players castle moves,
     * by checking if the king and respective rooks haven't moved.
     * If the positions between the king and the rook are not occupied.
     * And that the new position of the King is not under attack
     * 
     * @param legalMoves Players legal moves
     * @param opponetMoves Opponents legal moves
     * @return List of the players castle moves
     */
    public abstract List<Move> getKingCastleMoves(List<Move> legalMoves, List<Move> opponetMoves);

}
