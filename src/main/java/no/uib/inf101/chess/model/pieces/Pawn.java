package no.uib.inf101.chess.model.pieces;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.AttackMove;
import no.uib.inf101.chess.model.move.MajorMove;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.move.PawnDoubleStep;
import no.uib.inf101.grid.CellPosition;

/**
 * INSPIRED
 * The Code for calculating the legal moves of the pawn was inspired by these
 * three videos
 * 
 * source1:
 * https://www.youtube.com/watch?v=ItdijoP99pQ&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=10&ab_channel=SoftwareArchitecture%26Design
 * source2:
 * https://www.youtube.com/watch?v=LGJ5_Ka4UTE&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=11&ab_channel=SoftwareArchitecture%26Design
 * source3:
 * https://www.youtube.com/watch?v=OaWkMumR1I0&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=12&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public class Pawn extends Piece {
    private final static CellPosition[] CANDIDATE_POSITIONS = { new CellPosition(1, 0),
            new CellPosition(2, 0), new CellPosition(1, 1), new CellPosition(1, -1) };

    public Pawn(CellPosition pos, Alliance pieceAlliance) {
        super(pos, pieceAlliance, true);
    }

    public Pawn(CellPosition pos, Alliance pieceAlliance, boolean firstMove) {
        super(pos, pieceAlliance, firstMove);
    }

    @Override
    public String getPieceType() {
        return "Pawn";
    }

    @Override
    public List<Move> calculateLegalMoves(ChessBoard board) {
        CellPosition candidateDestinationPos;
        final List<Move> legalMoves = new ArrayList<>();

        for (CellPosition currentCandidate : CANDIDATE_POSITIONS) {
            int row = this.pos.row() + (this.pieceAlliance.getDirection() * currentCandidate.row());
            int col = this.pos.col() + currentCandidate.col();
            candidateDestinationPos = new CellPosition(row, col);

            if (!board.positionIsOnGrid(candidateDestinationPos)) {
                continue;
            }

            if (currentCandidate.row() == 1 && currentCandidate.col() == 0 &&
                    !board.isPositionOccupied(candidateDestinationPos)) {
                legalMoves.add(new MajorMove(board, this, candidateDestinationPos));
            } else if (currentCandidate.row() == 2 && this.isPawnStartingRow() &&
                    this.firstMove && !board.isPositionOccupied(candidateDestinationPos)) {
                int rowBehindCandidateDest = this.pos.row() + (this.pieceAlliance.getDirection());
                CellPosition behindCandidatePos = new CellPosition(rowBehindCandidateDest, col);

                if (!board.isPositionOccupied(behindCandidatePos)) {
                    legalMoves.add(new PawnDoubleStep(board, this, candidateDestinationPos));
                }
            } else if (currentCandidate.col() == 1 || currentCandidate.col() == -1) {
                if (board.isPositionOccupied(candidateDestinationPos)) {
                    Piece pieceAtDestination = board.getPiece(candidateDestinationPos);
                    Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();
                    if (this.pieceAlliance != pieceAlliance) {
                        legalMoves.add(new AttackMove(board, this, candidateDestinationPos, pieceAtDestination));
                    }
                } else if (board.getEnPassantPawn() != null) {
                    Pawn pawnAtPos = board.getEnPassantPawn();
                    CellPosition nextToThis = new CellPosition(this.pos.row(), col);
                    if (pawnAtPos.getPiecePosition().equals(nextToThis)) {
                        if (this.pieceAlliance != pawnAtPos.getPieceAlliance()) {
                            legalMoves.add(new AttackMove(board, this, candidateDestinationPos, pawnAtPos));
                        }
                    }
                }
            }
        }
        return legalMoves;
    }

    // Checks if pawn is at starting row
    private boolean isPawnStartingRow() {
        return (this.pieceAlliance.isWhite() && this.pos.row() == 6) ||
                (this.pieceAlliance.isBlack() && this.pos.row() == 1);
    }

    @Override
    public Piece movePiece(Move move) {
        return new Pawn(move.getDestination(), move.getMovedPiece().getPieceAlliance(), false);
    }
}
