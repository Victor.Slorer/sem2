package no.uib.inf101.chess.model;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.controller.ControllableChessModel;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.move.MoveFactory;
import no.uib.inf101.chess.model.move.MoveTransition;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.chess.view.ViewableChessModel;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

public class ChessModel implements ViewableChessModel, ControllableChessModel {
    private ChessBoard board;
    private Piece selectedPiece;
    private Move previousMove;
    private GameState gameState;
    private CellPosition pressedTile;
    private List<Move> selectedPieceMoves;

    public ChessModel(ChessBoard board) {
        this.board = board;
        this.selectedPiece = null;
        this.previousMove = null;
        this.gameState = GameState.ACTIVE_GAME;
        this.pressedTile = null;
        this.selectedPieceMoves = null;
    }

    @Override
    public void selectPiece(CellPosition pos) {
        this.pressedTile = pos;
        if (isCurrentPlayerPieceAtPos(pos)) {
            this.selectedPiece = this.board.getPiece(pos);
            getPieceMoves(selectedPiece);
        } else {
            this.selectedPiece = null;
        }
    }

    @Override
    public GridDimension getDimensions() {
        return board.getBuilder();
    }

    @Override
    public GameState getGameState() {
        return this.gameState;
    }

    @Override
    public Iterable<GridCell<Piece>> getTilesOnBoard() {
        return board.getBuilder();
    }

    @Override
    public Iterable<Piece> getPiecesOnBoard() {
        return board.getAllPieces();
    }

    @Override
    public Iterable<Move> getLegalMovesOfPiece() {
        if (selectedPiece != null) {
            return this.selectedPieceMoves;
        }
        return null;
    }

    @Override
    public String getCurrentPlayerAlliance() {
        return this.board.getCurrentPlayer().getPlayerAlliance().toString();
    }

    @Override
    public boolean pressedTileIsMove(CellPosition pos) {
        for (Move move : selectedPieceMoves) {
            CellPosition movePos = move.getDestination();
            if (movePos.equals(pos)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Move getPreviousMove() {
        return previousMove;
    }

    @Override
    public boolean pieceAtPosition(CellPosition pos) {
        Piece piece = board.getPiece(pos);
        if (piece == null) {
            return false;
        } else if (piece.getPieceAlliance() == board.getCurrentPlayer().getPlayerAlliance()) {
            return true;
        }
        return false;
    }

    @Override
    public CellPosition getPressedTile() {
        return this.pressedTile;
    }

    @Override
    public void playerMakeMove(CellPosition destination) {
        MoveTransition transition = movePiece(destination);
        if (isMoveLegal(transition)) {
            setNewBoard(transition);
            checkStateOfGame(transition);
        }
    }

    private void checkStateOfGame(MoveTransition transition) {
        if (transition.getBoard().getCurrentPlayer().isInCheckMate()) {
            this.gameState = GameState.CHECKMATE;
        } else if (transition.getBoard().getCurrentPlayer().isInStaleMate()) {
            this.gameState = GameState.STALEMATE;
        }
    }

    private boolean isMoveLegal(MoveTransition transition) {
        return transition.getMoveStatus().isLegal();
    }

    private MoveTransition movePiece(CellPosition destination) {
        return this.board.getCurrentPlayer()
                .makeMove(MoveFactory.getMove(this.board, this.selectedPiece.getPiecePosition(), destination));
    }

    private void setNewBoard(MoveTransition transition) {
        this.board = transition.getBoard();
        this.selectedPiece = null;
        this.previousMove = transition.getMove();
    }

    private void getPieceMoves(Piece piece) {
        this.selectedPieceMoves = new ArrayList<>();
        for (Move move : this.board.getCurrentPlayer().getLegalMoves()) {
            if (move.getMovedPiece().equals(piece)) {
                this.selectedPieceMoves.add(move);
            }
        }
    }

    private boolean isCurrentPlayerPieceAtPos(CellPosition pos) {
        return this.board.getPiece(pos) != null
                && this.board.getPiece(pos).getPieceAlliance() == this.board.getCurrentPlayer()
                        .getPlayerAlliance();
    }

    @Override
    public void resetBoard() {
        this.board = ChessBoard.createStandardBoard();
        this.gameState = GameState.ACTIVE_GAME;
        this.previousMove = null;
    }
}
