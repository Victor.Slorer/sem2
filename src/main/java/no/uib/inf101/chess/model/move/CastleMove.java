package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.chess.model.pieces.Rook;
import no.uib.inf101.grid.CellPosition;

/**
     * Creating two static classes for each of the castle moves was inspired by this video
     * 
     * source: https://www.youtube.com/watch?v=01fpv16NSJY&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=24&ab_channel=SoftwareArchitecture%26Design
     * 24.4.2023
     */

public class CastleMove extends Move {
    protected final Rook castleRook;
    protected final CellPosition castleRookPos;
    protected final CellPosition castleRookDest;

    public CastleMove(ChessBoard board, Piece movedPiece, CellPosition destination,
            Rook castleRook, CellPosition castleRookPos, CellPosition castleRookDest) {
        super(board, movedPiece, destination);
        this.castleRook = castleRook;
        this.castleRookPos = castleRookPos;
        this.castleRookDest = castleRookDest;
    }

    @Override
    public String getMoveType() {
        return "CastleMove";
    }

    // TODO inspired
    @Override
    public ChessBoard execute() {
        final Builder builder = new Builder(8, 8);

        // gets and sets all the pieces that are not moved
        for (Piece piece : this.board.getAllPieces()) {
            if (!this.movedPiece.equals(piece) && !this.castleRook.equals(piece)) {
                builder.setPiece(piece);
            }
        }

        // Sets the king at the new position
        builder.setPiece(this.movedPiece.movePiece(this));
        // Sets the rook at the new position
        builder.setPiece(new Rook(castleRookDest, castleRook.getPieceAlliance(), false));
        // Sets the next move maker
        builder.setMoveMaker(this.board.getCurrentPlayer().getOpponent().getPlayerAlliance());

        // builds and returns the new ChessBoard
        return builder.build();
    }

    // Static classes used to differentiate between King and Queen side castle moves
    public static class KingSideCastleMove extends CastleMove {

        public KingSideCastleMove(ChessBoard board, Piece movedPiece, CellPosition destination, Rook castleRook,
                CellPosition castleRookPos, CellPosition castleRookDest) {
            super(board, movedPiece, destination, castleRook, castleRookPos, castleRookDest);
        }
    }

    public static class QueenSideCastleMove extends CastleMove {

        public QueenSideCastleMove(ChessBoard board, Piece movedPiece, CellPosition destination, Rook castleRook,
                CellPosition castleRookPos, CellPosition castleRookDest) {
            super(board, movedPiece, destination, castleRook, castleRookPos, castleRookDest);
        }
    }

}
