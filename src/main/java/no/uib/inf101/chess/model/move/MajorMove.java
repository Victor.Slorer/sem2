package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

public final class MajorMove extends Move{

    public MajorMove(ChessBoard board, Piece movedPiece, CellPosition destination) {
        super(board, movedPiece, destination);
    }

    @Override
    public String getMoveType() {
        return "MajorMove";
    }

    // TODO inspired
    @Override
    public ChessBoard execute() {
        final Builder builder =  new Builder(8, 8);

        // gets and sets all the players pieces that haven't been moved
        for (Piece piece : this.board.getCurrentPlayer().getActivePieces()) {
            if (!this.movedPiece.equals(piece)) {
                builder.setPiece(piece);
            }
        }

        // gets and sets all the opponents pieces
        for (Piece piece : this.board.getCurrentPlayer().getOpponent().getActivePieces()) {
            builder.setPiece(piece);
        }

        // sets the moved piece at the new position
        builder.setPiece(this.movedPiece.movePiece(this));
        // Sets the next move maker
        builder.setMoveMaker(this.board.getCurrentPlayer().getOpponent().getPlayerAlliance());

        // Builds and returns a new board
        return builder.build();
    }
}
