package no.uib.inf101.chess.model.pieces;

import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.grid.CellPosition;

/**
 * TAKEN
 * Took the Piece class from this video
 * 
 * source: https://www.youtube.com/watch?v=GKJ3yYFCJO4&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=3&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 * 
 * The move piece function was taken from this video
 * 
 * source: https://www.youtube.com/watch?v=YGHHyosQT3s&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=22&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public abstract class Piece {
    protected final CellPosition pos;
    protected final Alliance pieceAlliance;
    protected final boolean firstMove;

    Piece(CellPosition pos, Alliance pieceAlliance, boolean firstMove) {
        this.pos = pos;
        this.pieceAlliance = pieceAlliance;
        this.firstMove = firstMove;
    }

    /**
     * Gets the alliance of the piece
     * 
     * @return the piece alliance
     */
    public Alliance getPieceAlliance() {
        return this.pieceAlliance;
    }

    /**
     * Gets the position of the piece
     * 
     * @return the piece position
     */
    public CellPosition getPiecePosition() {
        return this.pos;
    }

    /**
     * checks if the piece has moved
     * 
     * @return true if the piece has not moved, false otherwise
     */
    public boolean isFirstMove() {
        return this.firstMove;
    }

    /**
     * Gets the piece type
     * 
     * @return the piece type
     */
    public abstract String getPieceType();

    /**
     * Calculates the legal moves of the piece using the given board.
     * Each piece has a different way calculating their legal moves.
     * 
     * @param board the Chessboard used to calculate the legal moves of the piece
     * 
     * @return List of moves
     */
    // TODO inspired
    public abstract List<Move> calculateLegalMoves(ChessBoard board);

    /**
     * Creates a new piece at the destination of the move on the board
     * 
     * @param move that moves the piece
     * 
     * @return the new piece at the new position
     */
    // TODO taken
    public abstract Piece movePiece(Move move);

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((pos == null) ? 0 : pos.hashCode());
        result = prime * result + ((pieceAlliance == null) ? 0 : pieceAlliance.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Piece other = (Piece) obj;
        if (pos == null) {
            if (other.pos != null)
                return false;
        } else if (!pos.equals(other.pos))
            return false;
        if (pieceAlliance != other.pieceAlliance)
            return false;
        return true;
    }
}
