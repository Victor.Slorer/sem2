package no.uib.inf101.chess.model.pieces;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.move.AttackMove;
import no.uib.inf101.chess.model.move.MajorMove;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.grid.CellPosition;

/**
 * INSPIRED
 * Reused the code from the Queen for calculating the legal moves and just removed the while loop
 * 
 * 
 */

public class King extends Piece {
    private final static CellPosition[] CANDIDATE_POSITIONS = { new CellPosition(-1, 0),
            new CellPosition(0, 1), new CellPosition(1, 0), new CellPosition(0, -1),
            new CellPosition(1, 1), new CellPosition(-1, 1), new CellPosition(1, -1),
            new CellPosition(-1, -1) };

    private final boolean isCastled;

    public King(CellPosition pos, Alliance pieceAlliance) {
        super(pos, pieceAlliance, true);
        this.isCastled = false;
    }

    public King(CellPosition pos, Alliance pieceAlliance, boolean firstMove, boolean isCastled) {
        super(pos, pieceAlliance, firstMove);
        this.isCastled = isCastled;
    }

    @Override
    public String getPieceType() {
        return "King";
    }

    /**
     * returns the isCastled boolean, which is false if the king is not castled,
     * true if it is.
     * 
     * @return true if king is castled, false if not
     */
    public boolean isCastled() {
        return this.isCastled;
    }

    @Override
    public List<Move> calculateLegalMoves(ChessBoard board) {
        CellPosition candidateDestinationPos;
        final List<Move> legalMoves = new ArrayList<>();

        for (CellPosition currentCandidate : CANDIDATE_POSITIONS) {
            int row = this.pos.row() + currentCandidate.row();
            int col = this.pos.col() + currentCandidate.col();
            candidateDestinationPos = new CellPosition(row, col);

            if (board.positionIsOnGrid(candidateDestinationPos)) {
                if (!board.isPositionOccupied(candidateDestinationPos)) {
                    legalMoves.add(new MajorMove(board, this, candidateDestinationPos));
                } else {
                    Piece pieceAtDestination = board.getPiece(candidateDestinationPos);
                    Alliance pieceAlliance = pieceAtDestination.getPieceAlliance();

                    if (this.pieceAlliance != pieceAlliance) {
                        legalMoves.add(new AttackMove(board, this, candidateDestinationPos, pieceAtDestination));
                    }
                }
            }
        }
        return legalMoves;
    }

    @Override
    public Piece movePiece(Move move) {
        return new King(move.getDestination(), move.getMovedPiece().getPieceAlliance(), false,
                checkMoveType(move.getMoveType()));
    }

    private boolean checkMoveType(String moveType) {
        if (moveType == "CastleMove") {
            return true;
        } else {
            return false;
        }
    }

}
