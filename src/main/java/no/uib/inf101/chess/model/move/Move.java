package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

/**
 * INSPIRED
 * The move class and the attack and major move classes were inspired by this video
 * 
 * source: https://www.youtube.com/watch?v=nU9suN_CiHU&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=6&ab_channel=SoftwareArchitecture%26Design
 * 24.4.2023
 */

public abstract class Move {
    
    final ChessBoard board;
    final Piece movedPiece;
    final CellPosition destination;

    public Move(ChessBoard board, Piece movedPiece, CellPosition destination){
        this.board = board;
        this.movedPiece = movedPiece;
        this.destination = destination;
    }

    /**
     * Gets the destination of the move
     * 
     * @return the destination
     */
    public CellPosition getDestination(){
        return this.destination;
    }

    /**
     * Gets the moved piece
     * 
     * @return the moved piece
     */
    public Piece getMovedPiece(){
        return this.movedPiece;
    }

    /**
     * Gets the current position of the piece that is to be moved
     * 
     * @return current pos of the piece
     */
    public CellPosition getCurrentPos(){
        return this.movedPiece.getPiecePosition();
    }

    /**
     * Gets the move type
     * 
     * @return the move type
     */
    public abstract String getMoveType();


    /**
     * The execute method is inspired by this video
     * 
     * https://www.youtube.com/watch?v=JsI8K1ONcPU&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt&index=21&ab_channel=SoftwareArchitecture%26Design
     * 24.4.2023
     */
    /**
     * Executes the move using the builder, 
     * Then creates a new board with the executed move.
     * 
     * @return the new board
     */
    public abstract ChessBoard execute();
}
