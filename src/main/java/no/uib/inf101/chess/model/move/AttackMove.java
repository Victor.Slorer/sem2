package no.uib.inf101.chess.model.move;

import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessBoard.Builder;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

public final class AttackMove extends Move{
    final Piece attackedPiece;

    public AttackMove(ChessBoard board, Piece movedPiece, CellPosition destination, Piece attackedPiece) {
        super(board, movedPiece, destination);
        this.attackedPiece = attackedPiece;
    }

    @Override
    public String getMoveType() {
        return "AttackMove";
    }

    // TODO inspired
    @Override
    public ChessBoard execute() {
        final Builder builder =  new Builder(8, 8);

        // Gets and sets all the players pieces that haven't been moved
        for (Piece piece : this.board.getCurrentPlayer().getActivePieces()) {
            if (!this.movedPiece.equals(piece)) {
                builder.setPiece(piece);
            }
        }

        // Gets and sets all the opponents pieces that haven't been captured
        for (Piece piece : this.board.getCurrentPlayer().getOpponent().getActivePieces()) {
            if (!this.attackedPiece.equals(piece)) {
                builder.setPiece(piece);
            }
        }

        // Sets the moved piece at the new position
        builder.setPiece(this.movedPiece.movePiece(this));

        // Sets the next move maker
        builder.setMoveMaker(this.board.getCurrentPlayer().getOpponent().getPlayerAlliance());

        // returns the new board
        return builder.build();
    }
    
}
