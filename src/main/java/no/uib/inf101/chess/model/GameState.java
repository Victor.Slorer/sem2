package no.uib.inf101.chess.model;

public enum GameState {
    ACTIVE_GAME,
    CHECKMATE,
    STALEMATE
}
