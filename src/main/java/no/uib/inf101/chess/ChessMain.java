package no.uib.inf101.chess;


import javax.swing.JFrame;

import no.uib.inf101.chess.controller.ChessController;
import no.uib.inf101.chess.model.ChessBoard;
import no.uib.inf101.chess.model.ChessModel;
import no.uib.inf101.chess.view.ChessView;


public class ChessMain {
    public static final String WINDOW_TITLE = "INF101 Chess";

    public static void main(String[] args) {
        ChessBoard board = ChessBoard.createStandardBoard();
        ChessModel model = new ChessModel(board);
        ChessView view = new ChessView(model);
        new ChessController(model, view);

        System.out.println(board.prettyString());

        // The JFrame is the "root" application window.
        // We here set som properties of the main window,
        // and tell it to display our tetrisView
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Here we set which component to view in our window
        frame.setContentPane(view);

        // Call these methods to actually display the window
        frame.pack();
        frame.setVisible(true);
    }
}