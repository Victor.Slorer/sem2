package no.uib.inf101.chess.view;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

/** Tok koden fra lab 4 
 * Koden er helt lik ettersom at begge bruker samme 
 * parametre
*/

public class CellPositionToPixelConverter {
  private Rectangle2D box;
  private GridDimension gd;
  private double margin;

  // Constructor
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin, int width, 
  int height, double outerMargin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  /**
   * Gets the pixel bounds for the cell using the position of the 
   * cell within the grid
   * 
   * @param pos to get pixel bounds for
   * @return a rectangle2D with the correct coordinates and dimensions
   */
  public Rectangle2D getBoundsForCell(CellPosition pos){
    double rows = gd.rows();
    double cols = gd.cols();
    double cellWidth = (box.getWidth() - (this.margin * (cols + 1))) / cols;
    double cellHeight = (box.getHeight() - (this.margin * (rows + 1))) / rows;
    double cellX = box.getX() + (this.margin * (pos.col() + 1)) + (cellWidth * pos.col());
    double cellY = box.getY() + (this.margin * (pos.row() + 1)) + (cellHeight * pos.row());
    Rectangle2D cell = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return cell;
  }
}
