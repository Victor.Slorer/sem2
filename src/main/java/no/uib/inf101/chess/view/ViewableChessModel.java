package no.uib.inf101.chess.view;

import no.uib.inf101.chess.model.GameState;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

public interface ViewableChessModel {
    
    /**
     * Gets the dimensions of the tetris board
     * 
     * @return the dimensions of the board
     */
    GridDimension getDimensions();

    /**
     * Gets an iterator for the tiles on the board
     * 
     * @return an iterable of the tiles on the board
     */
    Iterable<GridCell<Piece>> getTilesOnBoard();
    
    /**
     * Gets an iterator for the pieces on the board
     * 
     * @return an iterable of the pieces
     */
    Iterable<Piece> getPiecesOnBoard();

    /**
     * Gets an iterable for the legal moves
     * of the selected piece
     * 
     * @return an iterable of the piece's moves
     */
    Iterable<Move> getLegalMovesOfPiece();

    /**
     * Gets the previous move made
     * 
     * @return the previous move made
     */
    Move getPreviousMove();

    /**
     * Gets the state of the game 
     * represented by the enum GameState
     * 
     * @return GameState
     */
    GameState getGameState();
    
    /**
     * Gets the pressed tile on the board
     * 
     * @return the pressed tile on the board
     */
    CellPosition getPressedTile();
    
    /**
     * Gets the current player's alliance
     * 
     * @return the current player's alliance
     */
    String getCurrentPlayerAlliance();

}
