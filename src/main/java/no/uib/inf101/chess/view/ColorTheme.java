package no.uib.inf101.chess.view;

import java.awt.Color;

import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;


public interface ColorTheme {
    /**
     * Gets the color of the Tetromino cells 
     * based on the cell position 
     * 
     * @param pos that represents the position of the cell
     * @return a color based on the given Character value
     */
    public Color getCellColor(CellPosition pos);

    /**
     * Gets the color of the frame
     * 
     * @return the color of the fram
     */
    public Color getFrameColor();

    /**
     * Gets the backgroundcolor of the game
     * 
     * @return the backgroundcolor of the game
     */
    public Color getBackgroundColor();

    /**
     * Gets the background color of the game over screen
     * 
     * @return the background color of the game over screen
     */
    public Color getGameOverColor();
    
    /**
     * Gets the color for the Game Over string 
     * 
     * @return the color for the Game Over string 
     */
    public Color getGameOverStringColor();

    /**
     * Gets the image of the piece
     * 
     * @param piece The piece 
     * 
     * @return path of the image
     */
    public String getPieceImage(Piece piece);

    /**
     * Gets the color of the pressed cell
     * 
     * @return the color of the pressed cell
     */
    public Color getPressedCellColor();

    /**
     * Gets the color for the previous move made
     * 
     * @return color for the previous move made
     */
    public Color getPreviousMoveColor();
}
