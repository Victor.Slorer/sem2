package no.uib.inf101.chess.view;

import javax.swing.JPanel;

import no.uib.inf101.chess.model.GameState;
import no.uib.inf101.chess.model.move.Move;
import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Dimension;
//import java.awt.Font;
import java.awt.Font;

public class ChessView extends JPanel {
  private ViewableChessModel model;
  private int width = 600;
  private int height = 600;
  private static final double OUTERMARGIN = 30;
  private static final double INNERMARGIN = 0;
  private ColorTheme colorTheme;

  // Constructor
  public ChessView(ViewableChessModel model) {
    this.model = model;
    this.colorTheme = new DefaultColorTheme();
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(width, height));
    this.setBackground(colorTheme.getBackgroundColor());
  }

  // The paintComponent method is called by the Java Swing framework every time
  // either the window opens or resizes, or we call .repaint() on this object.
  // Note: NEVER call paintComponent directly yourself
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    width = this.getWidth();
    height = this.getHeight();
    drawGame(g2);
  }

  private void drawGame(Graphics2D g2) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;

    // The frame of the game, this will be the background
    Rectangle2D shape = new Rectangle2D.Double(x, y, width, height);
    g2.setColor(Color.GRAY);
    g2.fill(shape);
    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(shape, model.getDimensions(), INNERMARGIN,
        this.getWidth(), this.getHeight(), OUTERMARGIN);
    drawCells(g2, model.getTilesOnBoard(), converter, colorTheme, model.getPressedTile());
    drawPreviousMove(g2, model.getPreviousMove(), converter, colorTheme);
    drawPieces(g2, model.getPiecesOnBoard(), converter, colorTheme);
    drawPieceLegalMoves(g2, model.getLegalMovesOfPiece(), converter, colorTheme);
    if (model.getGameState() != GameState.ACTIVE_GAME) {
      drawGameOverScreen(g2);
    }
  }

  private static void drawCells(Graphics2D g2, Iterable<GridCell<Piece>> iterable,
      CellPositionToPixelConverter converter, ColorTheme colorTheme, CellPosition pressedCell) {
    for (GridCell<Piece> cell : iterable) {
      Rectangle2D box = converter.getBoundsForCell(cell.pos());
      Color color = colorTheme.getCellColor(cell.pos());
      g2.setColor(color);
      g2.fill(box);
      if (cell.pos().equals(pressedCell)) {
        g2.setColor(colorTheme.getPressedCellColor());
        g2.draw(box);
      }
    }
  }

  private void drawPreviousMove(Graphics2D g2, Move previousMove, CellPositionToPixelConverter converter,
      ColorTheme colorTheme) {
    if (previousMove != null) {
      Rectangle2D box = converter.getBoundsForCell(previousMove.getCurrentPos());
      g2.setColor(colorTheme.getPreviousMoveColor());
      g2.fill(box);

      Rectangle2D box2 = converter.getBoundsForCell(previousMove.getDestination());
      g2.setColor(colorTheme.getPreviousMoveColor());
      g2.fill(box2);
    }
  }

  private void drawPieceLegalMoves(Graphics2D g2, Iterable<Move> legalMovesOfPiece,
      CellPositionToPixelConverter converter,
      ColorTheme colorTheme) {
    if (legalMovesOfPiece != null) {
      for (Move move : legalMovesOfPiece) {
        Rectangle2D box = converter.getBoundsForCell(move.getDestination());
        Color color = Color.RED;
        g2.setColor(color);
        g2.draw(box);
      }
    }
  }

  private void drawPieces(Graphics2D g2, Iterable<Piece> piecesOnBoard, CellPositionToPixelConverter converter,
      ColorTheme colorTheme) {
    for (Piece piece : piecesOnBoard) {
      Rectangle2D box = converter.getBoundsForCell(piece.getPiecePosition());
      BufferedImage img = Inf101Graphics.loadImageFromResources(colorTheme.getPieceImage(piece));
      double scale = (box.getWidth() + box.getHeight()) / (img.getWidth() + img.getHeight());
      Inf101Graphics.drawImage(g2, img, box.getX(), box.getY(), scale);
    }
  }

  private void drawGameOverScreen(Graphics2D g2) {
    String string = "";
    if (model.getGameState() == GameState.CHECKMATE) {
      string = " is in CheckMate";
    } else if (model.getGameState() == GameState.STALEMATE) {
      string = " is in StaleMate";
    }
    Rectangle2D gameOverScreen = new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight());
    g2.setColor(this.colorTheme.getGameOverColor());
    g2.fill(gameOverScreen);
    g2.setFont(new Font("Monospace", Font.BOLD, 50));
    g2.setColor(this.colorTheme.getGameOverStringColor());
    g2.drawString("Game Over", this.getWidth() / 2 - 140, this.getHeight() / 2 - 30);
    g2.setFont(new Font("Monospace", Font.BOLD, 30));
    g2.setColor(this.colorTheme.getGameOverStringColor());
    g2.drawString(model.getCurrentPlayerAlliance() + string, this.getWidth() / 2 - 180,
        this.getHeight() / 2 + 30);
    g2.setFont(new Font("Monospace", Font.BOLD, 30));
    g2.setColor(this.colorTheme.getGameOverStringColor());
    g2.drawString("Press the screen to play again", this.getWidth() / 2 - 230,
        this.getHeight() / 2 + 70);
  }

  public double getAppMargin() {
    return OUTERMARGIN;
  }

  public int getAppHeight() {
    return height;
  }

  public int getAppWidth() {
    return width;
  }
}
