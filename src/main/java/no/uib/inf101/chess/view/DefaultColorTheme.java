package no.uib.inf101.chess.view;

import java.awt.Color;

import no.uib.inf101.chess.model.pieces.Piece;
import no.uib.inf101.grid.CellPosition;

public class DefaultColorTheme implements ColorTheme {

    @Override
    public Color getCellColor(CellPosition pos) {
        int num = (pos.row() + pos.col()) % 2;
        Color color = switch (num) {
            case 1 -> Color.decode("#769656");
            case 0 -> Color.decode("#eeeed2");
            default -> throw new IllegalArgumentException(
                    "No available color for '" + num + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return new Color(0, 0, 0, 0);
    }

    @Override
    public Color getBackgroundColor() {
        return Color.decode("#444444");
    }

    @Override
    public Color getGameOverColor() {
        return new Color(0, 0, 0, 128);
    }

    @Override
    public Color getGameOverStringColor() {
        return Color.WHITE;
    }

    @Override
    public String getPieceImage(Piece piece) {
        String color = piece.getPieceAlliance().toString();
        String type = piece.getPieceType();
        String key = color + type;
        String img = switch (key) {
            case "WHITEKnight" -> "/whiteKnight.png";
            case "BLACKKnight" -> "/blackKnight.png";
            case "WHITEBishop" -> "/whiteBishop.png";
            case "BLACKBishop" -> "/blackBishop.png";
            case "WHITERook" -> "/whiteRook.png";
            case "BLACKRook" -> "/blackRook.png";
            case "WHITEQueen" -> "/whiteQueen.png";
            case "BLACKQueen" -> "/blackQueen.png";
            case "WHITEKing" -> "/whiteKing.png";
            case "BLACKKing" -> "/blackKing.png";
            case "WHITEPawn" -> "/whitePawn.png";
            case "BLACKPawn" -> "/blackPawn.png";



            default -> throw new IllegalArgumentException(
                    "No available color for '" + key + "'");
        };
        return img;
    }

    @Override
    public Color getPressedCellColor() {
        return Color.CYAN;
    }

    @Override
    public Color getPreviousMoveColor() {
        return new Color(178, 255, 102, 80);
    }
}
