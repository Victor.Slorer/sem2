package no.uib.inf101.grid;

/** I Grid har jeg brukt kode fra lab 4
 * spesifikt da ColorGrid
 * Jeg har brukt koden fra konstruktøren og get, set metodene
 * Jeg brukte også getCells koden i iterator
 * 
 * Fått litt hjelp av Tobias Skodven
 * Han ga meg tips om å lage to konstruktører
 */

 import java.util.ArrayList;
 import java.util.Iterator;
 import java.util.List;

import no.uib.inf101.chess.model.pieces.Piece;
 
 public class Grid<E> implements IGrid<E> {
     private int rows;
     private int cols;
     protected List<List<GridCell<E>>> grid;
 
     // Constructor with value null
     public Grid(int rows, int cols) {
         this.rows = rows;
         this.cols = cols;
         this.grid = new ArrayList<>();
 
         for (int i = 0; i < rows; i++) {
             this.grid.add(new ArrayList<>());
             for (int j = 0; j < cols; j++) {
                 List<GridCell<E>> row = this.grid.get(i);
                 row.add(new GridCell<E>(new CellPosition(i, j), null));
             }
         }
     }
 
     // Constructor with a given value
     public Grid(int rows, int cols, Piece piece) {
         this.rows = rows;
         this.cols = cols;
         this.grid = new ArrayList<>();
 
         for (int i = 0; i < rows; i++) {
             this.grid.add(new ArrayList<>());
             for (int j = 0; j < cols; j++) {
                 List<GridCell<E>> row = this.grid.get(i);
                 row.add(new GridCell<E>(new CellPosition(i, j), piece));
             }
         }
     }
 
     @Override
     public int rows() {
         return rows;
     }
 
     @Override
     public int cols() {
         return cols;
     }

     @Override
    public Boolean isPositionOccupied(CellPosition pos) {
        if (this.getPiece(pos) == null) {
            return false;
        } else {
            return true;
        }
    }
 
     @Override
     public Iterator<GridCell<Piece>> iterator() {
         ArrayList<GridCell<Piece>> liste = new ArrayList<GridCell<Piece>>();
         for (int i = 0; i < rows; i++) {
             for (int j = 0; j < cols; j++) {
                 CellPosition pos = new CellPosition(i, j);
                 Piece piece = getPiece(pos);
                 liste.add(new GridCell<Piece>(pos, piece));
             }
         }
         return liste.iterator();
     }
 
     @Override
     public void set(CellPosition pos, Piece piece) {
         this.grid.get(pos.row()).set(pos.col(), new GridCell<E>(pos, piece));
     }
 
     @Override
     public Piece getPiece(CellPosition pos) {
         GridCell<E> gridCell = this.grid.get(pos.row()).get(pos.col());
         return gridCell.piece();
     }

     @Override
     public boolean positionIsOnGrid(CellPosition pos) {
         if ((pos.row() < rows && pos.row() >= 0) && (pos.col() < cols && pos.col() >= 0)) {
             return true;
         } else {
             return false;
         }
     }

    @Override
    public List<List<GridCell<E>>> getGrid() {
        return this.grid;
    }
 }
