package no.uib.inf101.grid;

/** 
 * Holds the position of a cell in the grid, represented by two integers
 * one holding the row, another holding the collumn
 * 
 * @param row the row of the cell
 * @param col the col of the cell
*/
public record CellPosition(int row, int col) {}
