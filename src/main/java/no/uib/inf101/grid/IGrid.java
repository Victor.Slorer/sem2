package no.uib.inf101.grid;

import java.util.List;

import no.uib.inf101.chess.model.pieces.Piece;

public interface IGrid<E> extends GridDimension, Iterable<GridCell<Piece>> {
  /**
   * Sets the value of a position in the grid. A subsequent call to {@link #get}
   * with an equal position as argument will return the value which was set. The
   * method will overwrite any previous value that was stored at the location.
   * 
   * @param pos   the position in which to store the value
   * @param value the new value
   * @throws IndexOutOfBoundsException if the position does not exist in the grid
   */
  void set(CellPosition pos, Piece piece);

  /**
   * Gets the current value at the given coordinate.
   * 
   * @param pos the position to get
   * @return the value stored at the position
   * @throws IndexOutOfBoundsException if the position does not exist in the grid
   */
  Piece getPiece(CellPosition pos);

  /**
   * Checks if the position on the board is occupied
   * 
   * @param pos to be checked
   * @return true if the position is occupied, false otherwise
   */
  Boolean isPositionOccupied(CellPosition pos);

  /**
   * Gets the grid
   * 
   * @return the grid
   */
  List<List<GridCell<E>>> getGrid();

  /**
   * Reports whether the position is within bounds for this grid
   * 
   * @param pos position to check
   * @return true if the coordinate is within bounds, false otherwise
   */
  boolean positionIsOnGrid(CellPosition pos);
}
