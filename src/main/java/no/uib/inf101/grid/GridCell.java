package no.uib.inf101.grid;

import no.uib.inf101.chess.model.pieces.Piece;

/**
   * Holds the position and the value of a Cell in the Grid
   * 
   * @param pos the position of the cell
   * @param piece the piece on the cell
   */
  public record GridCell<E>(CellPosition pos, Piece piece) {}
